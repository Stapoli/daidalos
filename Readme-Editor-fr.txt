//---------------------------------------------------------------------
//						Da�dalos Level Editor
//---------------------------------------------------------------------
// Type: Editeur de niveaux pour Da�dalos
// Langage: Java
// En projet depuis: Septembre 2008
// Derni�re version: 16 D�cembre 2009
// Site: http://www.stephane-baudoux.com
//---------------------------------------------------------------------
//+Version 0.33
// - Ajout des pi�ges "Sol pi�g�" et "Arche"
//
//+Version 0.31
// - Le r�pertoire par d�faut de l'ouverture et de l'enregistrement des niveaux est maintenant la racine de l'ex�cutable.
// - Ajout des pi�ges Ventilation, Piques, tourelle de fl�ches et pilier de haches.
// - Ajout des objets 9mm et Lampe Torche.
// - Ajout des objets portes jaune, rouge, bleu et verte.
// - Ajout des objets cl�s jaune, rouge, bleu et verte.
// - Suppression du chemin d'acc�s pour Da�dalos �tant donn� que l'�diteur est maintenant dans le m�me dossier que le jeu.
//
//+Version 0.15
// - Ajout du t�l�porteur de fin de niveau.
// - Ajout de l'objet Porte.
//
//+Version 0.13
// - Ajout du fusil � pompe et des munitions dans la liste des objets utilisables.
// - Ajout de la possibilit� d'ouvrir les niveaux par double clic sur le niveau ou par glisser-d�poser sur l'ex�cutable pour Windows.
// - Durant le chargement d'une carte en m�moire, le titre de la fen�tre du programme indique "Chargement de la carte en cours...".
//
//+Version 0.1
// Fonctionnalit�s g�r�es:
// - Multi-langue (Fran�ais et Anglais).
// - Possibilit� d'ouvrir / modifier / enregistrer un niveau.
// - Placement assist� des blocs murs pour assurer un niveau coh�rent.
// - Possibilit� de placer tout les types d'objets g�r�s dans le jeu.
// - Possibilit� de choisir le d�but du niveau et l'orientation du joueur.
// - Possibilit� de tester le niveau en lan�ant Project D depuis l'interface de l'�diteur.
//
//---------------------------------------------------------------------
// 					Contr�les
//---------------------------------------------------------------------
//+Mode Mur:
// Clic Gauche 	-> Placer un bloc
// Clic Droit 	-> Retirer un bloc (retire �galement les objets associ�s)
//
//+Mode Objet:
// Clic Gauche 	-> Placer l'objet s�l�ctionn�
// Clic Droit 	-> Retirer l'objet
//
//+Mode Joueur:
// Clic Gauche 	-> Placer le d�part du niveau || Changer l'orientation de l'ic�ne 
// Clic Droit		-> Retirer le d�part du niveau
//
//---------------------------------------------------------------------	
//						Remarques
//---------------------------------------------------------------------
// - Le d�part du niveau est indispensable pour enregistrer ou tester
// le niveau
// - On ne peut placer des objets que sur des blocs non vides
// - Il peut y avoir plusieurs fin de niveaux
//---------------------------------------------------------------------