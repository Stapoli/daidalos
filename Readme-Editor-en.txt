//---------------------------------------------------------------------
//						Da�dalos Level Editor
//---------------------------------------------------------------------
// Type: Level editor for Da�dalos
// Language: Java
// In progress since: September, 2008
// Latest version: December 16th, 2009
// Web site: http://www.stephane-baudoux.com
//---------------------------------------------------------------------
//+Version 0.33
// - Added the traps "Trapped ground" and "Ark"
//
//+Version 0.31
// - The default directory for saving and loading is now the root directory of the program.
// - Added the traps: Ventilation, Pikes, Arrows Tower, Axes Pillar.
// - Added the objects: Handgun and Flashlight.
// - Added the objects: yellow, red, blue, green Doors.
// - Added the objects:  yellow, red, blue, green Magnetic cards.
// - Deletion of the access directory for Da�dalos since the editor is now with the game.
//
//+Version 0.15
// - Added the end level gate.
// - Added the object: Door.
//
//+Version 0.13
// - Added the objects: Shotgun and Shotgun ammo.
// - Added the possibility to open a level by double click on the file (Windows only).
// - Added a loading information on the window when a level is opened.
//
//+Version 0.1
// Fonctionnalities:
// - Multiple manguages (French and English).
// - Possibility to open / modify and save a level.
// - Assisted placement of the wall blocks to unsure a coherent level.
// - Possibility to put all the objects of the game.
// - Possibility to choose the start of the level.
// - Possibility to test the level by launching Project D from the editor interface. 
//
//---------------------------------------------------------------------
//						Controls
//---------------------------------------------------------------------
//+Wall mod:
// Left click 	-> Put a block
// Right click 	-> Delete a block (delete also the objects inside the block)
//
//+Object mod:
// Left click 	-> Put the selected object
// Right click 	-> Delete the object
//
//+Player mod:
// Left click 	-> Put the start icon || Changer the start icon orientation
// Right click	-> Delete the start icon
//
//---------------------------------------------------------------------	
//						Remarks
//---------------------------------------------------------------------
// - The start icon is essential in order to save or test the level.
// - Objects can be put only on non empty blocks.
// - There can be multiple end level.
//---------------------------------------------------------------------