//------------------------------------------------------------------
// Da�dalos D�mo Alpha
//------------------------------------------------------------------
// Type: FPS 3D
// Langage: C++
// Biblioth�ques: OpenGL, SDL, SDL_Mixer, TinyXML
// En projet depuis: Juillet 2008
// Derni�re version: 16 D�cembre 2009
// Site: http://www.stephane-baudoux.com
//------------------------------------------------------------------
//
//+Version 0.85
// Nouvelles fonctionnalit�s:
// -Ajout du pi�ge "Sol pi�g�"
// -Ajout de l'objet "Arche"
//
// Modifications de fonctionnalit�s existantes:
// -Modification de la gestion du saut. Il est maintenant possible de l�g�rement d�vier sa trajectoire durant le saut.
// -R�duction du rayon d'action des pi�ges "Piques" et "Ventillation". Il est maintenant plus ais� de passer � c�ter d'eux sans forcement prendre des d�g�ts.
// -Ajout d'une distance minmale pour le d�clenchement des pi�ges.
// -Les portes v�rouill�es restent maintenant ouvertes lorsqu'elles sont d�bloqu�es.
// -Ajout d'un timer dans les menus pour �viter d'utiliser trop ressources pour rien.
//
// Corrections:
// -De l'animation des armes � feu avec des fps �lev�s.
// -De la gestion des projectiles avec des fps �lev�s.
// -Du saut o� il arrivait parfois qu'il ne fonctionne pas.
// -Des impacts g�n�r�s par des pi�ges coll�s aux murs.
// -Du placement des textures sur les pi�ges "mur de haches".
// -Des mipmaps qui �taient inatives depuis quelques versions. Cela devrait entra�ner un gain de performance.
//
// Il est � noter que les sources du projet sont maintenant disponibles sur mon site personnel. Le projet est disponible sous la licence CC BY NC SA 2.0 (FR) / 3.0 (US).
// J'esp�re que mon exp�rience personnelle dans la 3D sera profitable � quelqu'un et que les sources vous int�resseront. Elles ne sont pas parfaites et accusent leur un an et demi de modifications et ajouts donc il y a probablement certains points qui peuvent encore �tre am�lior�s, cependant elles me satisfassent enti�rement malgr�s les d�fauts qu'il pourrait y avoir :-)
//
//+Version 0.8
// Nouvelles fonctionnalit�s:
//  -Ajout des objets "Portes v�rouill�e" Jaune, Rouge, Vert et Bleu
// -Ajout des objets "Cartes magn�tique" Jaune, Rouge, Vert et Bleu pour ouvrir les portes v�rouill�es
// -Ajout des mod�les 3d du 9mm et de la lampe torche qui sont maintenant des objets ramassables
// -Ajout de l'objet "Plafond bas", qui demande au joueur d'�tre accroupi pour pouvoir passer
// -Ajout de 4 Pi�ges: "Piques", "Ventilation", "Tourelle de fl�ches" et "Pilier de haches"
// -Ajout des blessures sur le joueur et de la possibilit� de mourrir (les blessures provoquent des saignements)
// -Ajout de messages dans le jeu lors d'interactions avec les objets
// -Ajout d'un mode debug
// -Ajout d'une image d'intro au lancement du jeu
// -Ajout de plusieurs �l�ments dans l'interface
// -Ajout de l'�diteur de niveau avec le jeu, ils seront maintenant fournis ensembles
//
// Modifications de fonctionnalit�s existantes:
// -Grande mise � jour de la gestion des mods, qui propose en plus:
// 		Passage du fichier config.txt en vesion xml avec beaucoup d'options suppl�mentaires
// 		Possibilit� de d�finir des textures/textes personnalis�s
// 		Possibilit� de choisir les armes par d�faut et le nombre de munitions ainsi que la sant� de base
// 		Possibilit� de supprimer les textes de chargement
// 		Possibilit� de r�gler la lumi�re ambiante
// 		Possibilit� de supprimer les s�parateurs d'environnement
// 		Possibilit� de changer les coordonn�es et la taille des �l�ments de l'interface
// 		Possibilit� de choisir si le joueur commence avec la lampe torche ou les cartes magn�tique
// -La formule de calcul du son suivant la distance de la cible et du joueur est ajust�e
// -Passage du fichier option config.txt en version xml
// -Suppression des clignotements de certains objets � proximit� de la lampe torche
// -Il n'est plus possible de changer de direction durant un saut
// -Le Frustum Culling est d�sormais moins strict
// -Les probl�mes de sons non jou�s suivant certaines conditions son corrig�s
// -La r�solution 1920*1200 fonctionne correctement
// -Int�gration des particules et des impacts dans le Frustum Culling
// -Correction du probl�me de son lorsque celui-ci �tait mis � 0 depuis le jeu
// -Correction des impacts qui r�agissent maintenant correctement � la lampe torche
// -Les textures des menus sont maintenant en puissance de 2
// -Modification de la gestion des fichier lang pour permettre des ajouts plus ais�s
// -Correction d'une fuite m�moire r�currente qui �tait pr�sente depuis quelques versions
//
//+Version 0.7
// -Modification: Le projet change de nom pour prendre son nom d�finitif: Da�dalos
// -Correction: Les textures pour les objets pilliers et murrets sont maintenant d�pendantes des coordonn�es du pillier ou du murret. Des pilliers ou des murrets align�s auront donc des textures qui se suivent.
// -Correction: Erreur de texturing pour les murrets
// -Correction: Erreur dans le placement du joueur au point de d�part
// -Correction: Les impacts de balles apparaissent sur les pilliers
// -Ajout: Chaque arme poss�de maintenant un nombre maximum de balles transportables.
// 		9mm: 120
// 		Fusil � pompe: 64
// -Ajout: Possibilit� de placer des portes pour s�parer les salles. Les portes ont une taille modifiable.
// -Ajout: Possibilit� de sauter (d�lai de 600ms entre deux sauts)
// -Ajout: Possibilit� de finir les niveaux (enfin xD)
// -Ajout: Gestion de mods, afin de faciliter l'utilisation de niveaux par la communaut�. Pour plus d'info, consultez le fichier mods/lisez-moi.txt
// -Ajout: Ecran de chargement fonctionnel
// -Ajout: Moteur de particules 2D, avec les effets suivants:
// 		Fum� sur les canons des armes
// 		Fum� � l'impact des �l�ments non m�taliques
// 		Etincelles � l'impact des �l�ments autre que du bois
// 		Particules lumineuses sur les t�l�porteurs de d�but et de fin de niveau
// -Optimisations: Impl�mentation de l'algorithme de division de l'espace de type "Portail"
// -Optimisations: Impl�mentation du Viewing Frustum Culling
// -Optimisations: Impl�mentation d'un algo d'Occlusion Culling, des am�liorations sont encore possibles
//
//+Version 0.416
// -Correction: Le d�placement dans les menus n'est maintenant plus d�pendant des touches configur�es, mais est fix� sur les fl�ches directionnelles
// -Correction: La touche valider dans les menus fonctionne maintenant m�me sur les portables avec un pav� num�rique
// -Correction: R�solution erronn�e: 1900x1080 -> 1920x1080
// -Correction: Suppression de d�chargements de textures en doublon
// -Correction: Utiliser le fusil � pompe en continu pouvait dans certains cas provoquer des bogs de son
// -Ajout: La liste des r�solutions disponibles dans les options graphiques est maintenant filtr�e uniquement aux r�solutions accept�es par le syst�me
// -Ajout: Mise en place d'un changement dynamique de la r�solution, du plein �cran et de l'anticr�nelage depuis les options. Il n'est donc plus n�cessaire de red�marrer le jeu pour que ces r�glages prennent effet.
// -Ajout: Lorsque l'utilisateur veux mapper une touche d�j� utilis�e, un son est jou� pour le pr�venir que l'op�ration est interdit
// -Ajout: Si la r�solution choisie n'est pas support�e, le jeu passe automatiquement en 640x480 fen�tr�
// -Ajout: Gestion des impacts de balles
// -Ajout: Raccourcis configurables pour choisir le handgun ou le shotgun
// -Ajout: R�glage des impacts dans les options
// -Modification: Remaniement du fusil � pompe. Chaque coup provoque 6 tirs. Afin de maximiser les d�gats, il faut donc que tout les tirs touchent la cible.
//
//+Version 0.345
// -Ajout de la gestion des touches de la souris
// -Ajout d'un d�filement dans le menu contr�les
// -Ajout de trois nouvelles touches param�trables dans le menu Contr�les: Arme pr�c�dente, Arme suivante, Recharger
// -Ajout de la gestion des tirs:
// 		Chaque arme poss�de 6 statistiques: Force, Vitesse, Port�e, Pr�cision, taille du chargeur, vitesse de rechargement
// 		La pr�cision des tirs est d�pendante de la distance entre la cible et le joueur, de l'action du joueur (en mouvement, accroupi etc) et de la pr�cision de l'arme �quip�e
// -Ajout de sons sur les impacts des balles suivant les mat�riaux des surfaces cibl�es. La force du bruit d�pend de la distance entre joueur et la cible.
// -Ajout de deux armes, le 9mm et le fusil � pompe
// -Ajout de munitions pour le fusil � pompe
// -Ajout des cartouches pour le fusil � pompe parmi la liste des objets utilisables
// -Correction de bug: Le menu disparaissa�t sur on appuyait sur entrer dans le jeu avant d'apuyer sur echap
// -Correction de bug: Absence de d�chargement des textures lors du retour au menu principal
// -Correction de bug: Il n'est plus n�cessaire de red�marer le jeu pour que la qualit� des textes soit chang� lors d'un changement dans la qualit� des textures
// -Correction du chargeur de mod�les md2 et de certains mod�les pour prendre en compte le culling d'OpenGL
// -Optimisations: Activation du backface culling d'OpenGL
// -Optimisations: Mise en place d'une gestion d'optimisation de la qualit� graphiques des �l�ments affich�s suivant la distance qui les s�pare du joueur (LOD)
// -Les diff�rentes optimisations mises en place devraient entra�ner un gain de performance tr�s important, surtout sur les petites configs
//
//+Version 0.178
// -Suppression de l'option "tr�s haut" pour la qualit� de la lumi�re, option qui �tait tr�s peu int�ressante au vu de la sur-consommation engendr�e
// -Modification de la qualit� du rendu de la lumi�re dans certaines options graphiques
// -Modification de la mani�re dont sont enregistr�es les coordonn�es de d�part du joueur dans les niveaux
// -Modification de textures
// -Correction d'un bug dans la sauvegarde du niveau du gamma
// -Correction d'un bug qui demandait de relancer le jeu pour prendre en compte les changements de la sensibilit� de la souris depuis le menu option
// -Correction dans le nom d'une texture en qualit� minimale
// -Correction d'une texture manquante pour la qualit� de textures minimale
// -Ajout de la possibilit� de lancer un niveau depuis l'interface de l'�diteur de niveaux
// -Ajout de la possiblit� de s'accroupir
// -Ajout d'une gestion multi-environnements des niveaux
// -Ajout de deux nouveaux environnements
// -Ajout d'une gestion des bruits de pas suivant l'environnement sur lequel se trouve le joueur
// -La qualit� des textures choisie influe maintenant �galement sur la qualit� des textes
// -Suppression d'un grand nombre de fuites m�moires
//
//+Version 0.102
// -Ajout d'un r�glage pour le lissage du d�placement de la souris
// -Modification de l'impulsion du joueur lors du d�placement
//
//+Version 0.1
// Fonctionnalit�s g�r�es:
// -M�canique des niveaux
// -Gestion clavier
// -Gestion souris
// -Gestion de la langue
// -Gestion du son et de la musique
// -Gestion des menus
// -Gestion de l'interface
// -Gestion des collisions
// -Gestion des objets
// -Configuration possible des options graphiques
// -Configuration possible des touches
// -Fran�ais et Anglais g�r�s
//
//-------------------------------------------------------------------
//
//+En pr�vision pour la prochaine version:
// -Gestion des monstres
//
//-------------------------------------------------------------------
// Remerciements
//-------------------------------------------------------------------
//+Classes de chargement des fichiers .md2 et .tga:
// David Henry (http://tfc.duke.free.fr/)
//
//+Le parseur XML
// Lee Thomason, Yves Berquin et Andrew Ellerton pour tinyXML
// (http://www.grinninglizard.com/tinyxml/)
//
//+Les sons / musiques:
// Flick3r
// dynamedion (http://www.dynamedion.com)
// roocks (http://www.rookiecop.co.uk)
// Airborne Sound (http://www.airbornesound.com)
// SFX Bible (http://www.soundeffectsbible.com)
// BlastwaveFX (http://www.blastwavefx.com/)
// Shriek Productions (http://www.shriek-music.com/)
// Justine Angus
// Big Room Sound (http://www.bigroomsound.com/Home.html)
//
//+Les textures
// http://www.cgtextures.com/
//-------------------------------------------------------------------
