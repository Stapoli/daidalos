// -----------------------------------------------------------------
// Da�dalos: Code source
// -----------------------------------------------------------------
//
// Avant de modifier le code source, veuillez consulter le fichier
// Licence-fr.txt pr�sent dans le m�me dossier que ce fichier.
// Accepter la licence d'utilisation est indispensable pour pouvoir
// utiliser / modifier ce code source.
//
// -----------------------------------------------------------------
// Biblioth�ques n�cessaires
// -----------------------------------------------------------------
// 
// Pour pouvoir compiler ce code source, il est indispensable 
// d'avoir r�cup�r� au pr�alable les biblioth�ques suivantes:
// - OpenGL
// - SDL
// - SDL_mixer
// Les fichiers n�cessaires pour utiliser ces biblioth�ques ne sont
// pas fournis avec le code source de Da�dalos.
//
// Aucun fichier projet n'est fourni, n'�tant pas en mesure de 
// fournir un fichier projet pour chaque programme de chaque syst�me
// d'exploitation pour contenter tout le monde. Il vous faudra donc 
// cr�er votre propre fichier projet pour exploiter le code source.
//
// -----------------------------------------------------------------