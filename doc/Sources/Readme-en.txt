// -----------------------------------------------------------------
// Da�dalos: Source code
// -----------------------------------------------------------------
//
// Before starting using the source code, please read the Licence-en
// file located in the same directory as this file.
// In order to be able to use this source code, you must be agree
// to the term of use of the licence.
//
// -----------------------------------------------------------------
// Necessary libraries
// -----------------------------------------------------------------
// 
// In order to be able do compile the source code, it is necessary
// to get the following libraries:
// - OpenGL
// - SDL
// - SDL_mixer
// The files needed to use those libraries are not given with the
// source code of Da�dalos
//
// There is no project file given with the source code, as i will
// no be able to give a project file for each program of each OS.
// You will have to create your own project file, which means that
// you must know of to create a project file with your favorite
// editor in order to be able to use the source code.
//
// -----------------------------------------------------------------