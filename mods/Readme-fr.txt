//------------------------------------------------------------------
// Da�dalos D�mo Alpha
// - Gestion des mods
//------------------------------------------------------------------
// Da�dalos vous permet d'ajouter des mods au jeu.
// Le syst�me de mods permet pour le moment de:
// - Remplacer toutes les textures.
// - Remplacer tous les sons.
// - D�finir une liste de niveaux � franchir.
// - Choisir la musique pour chacun des niveaux.
// - Modifier les coordonn�es des �l�ments de l'interface.
// - Modifier la luminosit� ambiante.
// - Modifier la liste des objets fournis au joueur.
// - Modifier la vie et l'armure du joueur.
//
// Il n'est pas impossible que le syst�me de mods soit am�lior�
// par la suite et propose d'autres fonctionnalit�s.
//
// Tout les mods doivent �tre plac�s dans le dossier "mods"
// pr�sent � la racine du jeu. Ce dossier contient d�j� au
// minimum ce fichier et le dossier "Main", qui est le mod principal
// et est indispensable au bon fonctionnement du jeu.
//
//------------------------------------------------------------------
// Le contenu d'un mod:
//------------------------------------------------------------------
//	L'arborescence de vos mods doit respecter celle ci-dessous:
//	Les �l�ments entre [ ] sont facultatifs.
//
//
//	- R�pertoire_du_mod (R�pertoire)
//		-> levels (R�pertoire)
//		-> conf.xml
//		-> [ imgs ] (R�pertoire)
//		-> [ sounds ] (R�pertoire)
//	
//
//
//------------------------------------------------------------------
// R�pertoire_du_mod:
//------------------------------------------------------------------
// Le r�pertoire principal de votre mod.
//
//------------------------------------------------------------------
// Le r�pertoire levels:
//------------------------------------------------------------------
// Ce r�pertoire doit contenir l'ensemble des niveaux d�clar�s dans
// le fichier conf.xml.
//
//------------------------------------------------------------------
// Le fichier conf.xml:
//------------------------------------------------------------------
// Ce fichier contient les diff�rents r�glages du mod (nom, niveaux,
// sons etc)
// Pour un aper�u des r�glages disponibles vous pouvez consulter le 
// fichier mods/Main/conf.xml, qui contient tous les r�glages avec
// leurs valeur par d�faut. Une description de ces r�glages est
// �galement disponible � la fin de ce document.
//
//------------------------------------------------------------------
// Le dossier imgs:
//------------------------------------------------------------------
// Ce dossier sert � remplacer les textures pr�sentes dans le dossier 
// imgs � la racine du jeu par vos propres textures.
// Pour remplacer une texture, il suffit donc de respecter la m�me
// arborescence. Le jeu fera le travail de r�cup�rer le bon  
// fichier. Le format .tga est indispensable. Si vous utilisez un
// autre format, je jeu risque de planter.
// Il n'est pas n�cessaire de remplacer tout le dossier imgs, vous
// n'avez qu'� remplacer les fichiers pour les textures que vous
// voulez modifier.
// 
// Les dossiers 0 | 1 | 2 | 3 � la racine du dossier imgs regroupent
// des textures semblables mais de qualit� diff�rentes, utilis�es
// suivant les r�glages du joueur (0: qualit� tr�s faible - 
// 3: qualit� �lev�e).
// Si vous modifiez une texture pr�sente dans l'un de ces dossiers,
// pensez � la modifier pour les autres.
// La qualit� des textures est en g�n�ral comme suit:
// Dossier 0: Taille 2*2
// Dossier 1: Taille 128*128
// Dossier 2: Taille 256*256
// Dossier 3: Taille 512*512
// De mani�re g�n�rale, la qualit� inf�rieur a une taille 2 fois
// inf�rieur � la texture du dessus. La seule exception est le dossier 0
// qui a une taille de 2*2 sauf pour les impacts
//	
//------------------------------------------------------------------
// Le dossier sounds:
//------------------------------------------------------------------
// Ce dossier fonctionne sur le m�me principe que celui des textures.
// Pour remplacer un son / une musique, il suffit de reproduire 
// l'arborescence du dossier sounds dans votre dossier mods et de 
// fournir vos propore fichier wav.
// Le format est obligatoirement un fichier .wav
//
//------------------------------------------------------------------
// Les informations de configuration des mods:
//------------------------------------------------------------------
// G�n�ral:
// - name: Le nom du mod tel qu'il appara�tra dans le jeu.
// - number: Nombre de niveaux que contient le mod.
//
// Relatif aux niveaux:
// - position: La position du niveau dans la liste des niveaux.
//   La valeur minimale est 0 et la valeur maximale est le nombre
//   de niveaux - 1.
// - filename: Le nom du fichier dlf du niveau. Ne pas inclure
//   l'extension du fichier.
// - music: Le nom du fichier son utilis� pour la musique du niveau.
//   Ne pas inclure l'extension du fichier.
// - ambient_light: La valeur de la lumi�re ambiante du niveau en
//   pourcentage. La valeur de base est 100.
// - intersection_block: D�finit si oui ou non les s�parateurs
//   d'environnements sont activ�s pour le niveau. Les valeurs
//   accept�es sont enabled (activ�e) ou disabled (d�sactiv�e).
// - name: Le nom du niveau tel qu'il appara�tra � l'entr�e du
//   niveau.
//
// Relatif � l'interface:
// - text_x: Coordonn�e en x sur l'�cran du texte associ� � ic�ne. 
//   La valeur doit �tre comprise entre 0 (tout � gauche) et 1 (tout
//   � droite).
// - text_y: Coordonn�e en y sur l'�cran du texte associ� � ic�ne. 
//   La valeur doit �tre comprise entre 0 (tout en bas) et 1 (tout
//   en haut).
// - logo_x: Coordonn�e en x sur l'�cran de l'ic�ne. La valeur doit 
//   �tre comprise entre 0 (tout � gauche) et 1 (tout � droite).
// - logo_y: Coordonn�e en y sur l'�cran de l'ic�ne. La valeur doit 
//   �tre comprise entre 0 (tout en bas) et 1 (tout en haut).
// - logo_size_x: Taille en x du logo.
// - logo_size_y: Taille en y du logo.
//
// Relatif au joueur:
// - health_start: Vie de d�part du joueur. Doit �tre compris entre
//   1 et 100.
// - armor_start: Armure de d�part du joueur. Doit �tre compris entre
//   0 et 100.
// - flashlight_start: Si le joueur poss�de de base la lampe torche.
//   Les valeurs accept�es sont yes (oui) et no (non).
// - start: D�fini si une arme est fournie de base. Les valeurs 
//   accept�es sont yes (oui) et no (non).
// - ammo: D�fini les munitions fournies de base au joueur pour
//   l'arme donn�e.
//
// Divers:
// - loading_text: Permet d'activer ou non les textes de
//   chargement. Les valeurs accept�es sont enabled (activ�) et
//   disabled (d�sactiv�).
// - custom_textures: Permet d'activer ou non la recherche des 
//   textures personnalis�es dans le dossier du mod. Les valeurs 
//   accept�es sont enabled (activ�) et disabled (d�sactiv�).
// - custom_sounds: Permet d'activer ou non la recherche des 
//   sons personnalis�s dans le dossier du mod. Les valeurs 
//   accept�es sont enabled (activ�) et disabled (d�sactiv�).
//------------------------------------------------------------------
